package epsi.software.models;

public class Triangle extends ShapeAbs {

    private String symbol = "*";

    // Ajout d'un constructeur
    public Triangle(int size) {
        // Appel au constructeur de la classe parente pour initialiser la taille
        super(size);
    }

    // Nouveau constructeur avec une valeur par défaut pour size
    public Triangle() {
        // Appel au constructeur de la classe parente avec une taille par défaut de 3
        super(3);
    }

    @Override
    public void display() {
        System.out.println(' ');
        System.out.println(' ');
        System.out.println(' ');

        // Récupération de la taille depuis la classe parente
        Integer size = getSize();

        for (int i = 1; i <= size; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(symbol);
                System.out.print("  ");
            }
            System.out.println(' ');
        }
    }
}

