package epsi.software.models;

public abstract class ShapeAbs {

    private Integer size = 3;

    public ShapeAbs(int i) {
        size = i;
    }

    public abstract void display();

    public Integer getSize() {
        return size;
    }

    public Integer jumpLine() {
        return size;
    }
}
