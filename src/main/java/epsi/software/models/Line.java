package epsi.software.models;

public class Line extends ShapeAbs {
    private  String symbol = "*";

    // Ajout d'un constructeur
    public Line(int size) {
        // Appel au constructeur de la classe parente pour initialiser la taille
        super(size);
    }

    // Nouveau constructeur avec une valeur par défaut pour size
    public Line() {
        // Appel au constructeur de la classe parente avec une taille par défaut de 3
        super(3);
    }
    @Override
    public void display() {
        System.out.println(' ');
        System.out.println(' ');
        System.out.println(' ');
        for (int i = 1; i <= getSize(); i++) {
            System.out.print(symbol);
            System.out.print("  ");
        }
    }
}
