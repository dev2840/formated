package epsi.software.models;

public class Square extends ShapeAbs {
    private  String symbol = "*";

    // Ajout d'un constructeur
    public Square(int size) {
        // Appel au constructeur de la classe parente pour initialiser la taille
        super(size);
    }

    // Nouveau constructeur avec une valeur par défaut pour size
    public Square() {
        // Appel au constructeur de la classe parente avec une taille par défaut de 3
        super(3);
    }
    @Override
    public void display() {
        System.out.println(' ');
        System.out.println(' ');
        System.out.println(' ');

        Integer size = getSize();
        for (int i = 1; i <= size; i++) {
            for (int j = 1; j <= size; j++) {
                System.out.print(symbol);
                System.out.print("  ");
            }
            System.out.println(' ');
        }
    }
}
