// Déclaration du package
package epsi.software;

import epsi.software.models.Line;
import epsi.software.models.Square;
import epsi.software.models.Triangle;

/**
 * Hello world!
 *
 */
public class DisplayConsole
{
    public static void main( String[] args )
    {
        System.out.println(" ");
        System.out.println(" ");
        System.out.println("-------------- Les lignes --------------");

        Line line = new Line(5);
        line.display();
        line.display();
        line.display();

        Line line2 = new Line(8);
        line2.display();
        line2.display();

        System.out.println(" ");
        System.out.println(" ");
        System.out.println(" ");
        System.out.println("-------------- Les carres --------------");

        Square square = new Square(7);
        square.display();
        square.display();
        square.display();
        square.display();

        System.out.println(" ");
        System.out.println(" ");
        System.out.println("-------------- Les triangles --------------");

        Triangle triangle = new Triangle(5);
        triangle.display();
        triangle.display();
        triangle.display();

        Triangle triangle2 = new Triangle(6);
        triangle2.display();
        triangle2.display();

        System.out.println("Fin du code");
    }
}
